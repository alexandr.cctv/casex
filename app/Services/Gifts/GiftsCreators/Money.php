<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/7/18
 * Time: 11:56 PM
 */

namespace App\Services\Gifts\GiftsCreators;

use App\Models\MoneyGift;
use App\Models\UserTransaction;
use Illuminate\Support\Facades\Auth;

class Money extends GiftCreator
{
    protected $gifts;
    protected $maxGiftSum;

    public function __construct()
    {
        $this->gifts = MoneyGift::whereRaw('money_gifts.sum > money_gifts.gifted_sum')->get();
        $this->maxGiftSum = config('services.gifts.max_gift_money_sum');
        parent::__construct();
    }

    public function canPresent(): bool
    {
        $result = $this->gifts->isNotEmpty();

        if ($result) {
            $this->present();
        }

        return $result;
    }

    public function present()
    {
        $currentGiftSum = $this->getCurrentGiftSum();

        $this->presented = $this->user->transactions()->create([
            'amount' => $currentGiftSum,
            'type' => UserTransaction::TYPE_GIFT
        ]);

        $this->updateSums($currentGiftSum);
    }

    public function cancelLastPresent(int $entityId):?UserTransaction
    {
        $transaction = $this->user->transactions()->whereId($entityId)->first();

        if (!$transaction) {
            return null;
        }

        return $this->user->transactions()->create([
            'amount' => -$transaction->amount,
            'type' => UserTransaction::TYPE_CANCEL
        ]);
    }

    public function getMessage(): string
    {
        return 'You won '.$this->presented->amount.'$';
    }

    private function updateSums($currentGiftSum)
    {
        foreach ($this->gifts as $gift) {
            $diffSum = $gift->sum - $gift->gifted_sum;

            if ($diffSum <= $currentGiftSum) {
                $currentGiftSum = $currentGiftSum - $diffSum;
                $gift->gifted_sum = $gift->sum;
                $gift->save();
            } else {
                $gift->gifted_sum = $gift->gifted_sum + $currentGiftSum;
                $gift->save();
            }
        }
    }

    private function getCurrentGiftSum(): float
    {
        $availableSum = $this->gifts->sum('sum') - $this->gifts->sum('gifted_sum');
        if ($availableSum < 1) {
            return (float)$availableSum;
        }
        if ($availableSum < $this->maxGiftSum) {
            return (float)random_int(1, (int)$availableSum);
        }
        return (float)random_int(1, (int)$this->maxGiftSum);
    }
}