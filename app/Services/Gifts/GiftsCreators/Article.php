<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/8/18
 * Time: 12:11 AM
 */
namespace App\Services\Gifts\GiftsCreators;

use App\Models\Article as ObjectModel;

class Article extends GiftCreator
{
    protected $gifts;

    public function __construct()
    {
        $this->gifts = ObjectModel::where('available', '>', 0)->get();
        parent::__construct();
    }

    public function canPresent(): bool
    {
        $result = $this->gifts->isNotEmpty();
        if ($result) {
            $this->present();
        }
        return $result;
    }

    public function present()
    {
        $article = $this->gifts->random();
        $this->user->articles()->attach($article);
        $article->available--;
        $article->save();
        $this->presented = $article;
    }

    public function cancelLastPresent(int $entityId): ?int
    {
        $article = $this->user->articles()->whereId($entityId)->first();
        if (!$article) {
            return null;
        }

        return $this->user->articles()->detach($article->id);
    }

    public function getMessage(): string
    {
        return 'You won '.$this->presented->name;
    }
}