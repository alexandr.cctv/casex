<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/8/18
 * Time: 12:11 AM
 */
namespace App\Services\Gifts\GiftsCreators;

use App\Models\UserPoint;

class Point extends GiftCreator
{
    protected $maxGiftSum;

    public function __construct()
    {
        $this->maxGiftSum = config('services.gifts.max_gift_point_sum');
        parent::__construct();
    }

    public function canPresent(): bool
    {
        $this->present();
        return true;
    }

    public function present()
    {
        $this->presented = $this->user->points()->create([
            'amount' => random_int(1, $this->maxGiftSum)
        ]);
    }

    public function cancelLastPresent(int $entityId):?UserPoint
    {
        $point = $this->user->points()->whereId($entityId)->first();
        if (!$point) {
            return null;
        }
        return $this->user->points()->create([
            'amount' => -$point->amount
        ]);
    }

    public function getMessage(): string
    {
        return 'You won '.$this->presented->amount.' points';
    }
}