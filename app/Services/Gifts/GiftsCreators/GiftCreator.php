<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/8/18
 * Time: 12:26 AM
 */
namespace App\Services\Gifts\GiftsCreators;

use App\Services\Gifts\Interfaces\Gift;
use Illuminate\Support\Facades\Auth;

abstract class GiftCreator implements Gift
{
    protected $user;
    protected $presented;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function getGiftData(): array
    {
        return [
            'class' => get_called_class(),
            'entity_id' => $this->getEntityId(),
            'message' => $this->getMessage()
        ];
    }

    public function getEntityId(): int
    {
        return $this->presented->id;
    }
}