<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/7/18
 * Time: 11:13 PM
 */
namespace App\Services\Gifts;

use App\Services\Gifts\GiftsCreators\Money;
use App\Services\Gifts\GiftsCreators\Article;
use App\Services\Gifts\GiftsCreators\Point;
use App\Services\Gifts\Interfaces\Gift;

class Gifts
{
    const MONEY_TYPE = 'money';
    const POINT_TYPE = 'point';
    const OBJECT_TYPE = 'object';
    const AVAILABLE_TYPES = [
        self::MONEY_TYPE,
        self::POINT_TYPE,
        self::OBJECT_TYPE
    ];

    public function getRandom(): Gift
    {
        $currentType = self::AVAILABLE_TYPES[$this->getCurrentType()];

        switch ($currentType) {
            case self::MONEY_TYPE:
                $gift = new Money();
                break;
            case self::OBJECT_TYPE:
                $gift = new Article();
                break;
            case self::POINT_TYPE:
                $gift = new Point();
                break;
        }

        if (!$gift->canPresent()) {
            return $this->getRandom();
        }

        return $gift;
    }

    private function getCurrentType(): int
    {
        return random_int(0, count(self::AVAILABLE_TYPES)-1);
    }
}