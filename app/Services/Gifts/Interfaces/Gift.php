<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/7/18
 * Time: 11:33 PM
 */

namespace App\Services\Gifts\Interfaces;

interface Gift
{
    public function canPresent():bool;
    public function getGiftData():array;
    public function present();
    public function cancelLastPresent(int $entityId);
    public function getEntityId():int;
    public function getMessage():string;
}