<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/7/18
 * Time: 11:13 PM
 */
namespace App\Services\Gifts;

use Illuminate\Support\ServiceProvider;

class GiftsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Gifts::class);
    }
}
