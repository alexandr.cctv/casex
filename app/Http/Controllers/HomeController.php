<?php

namespace App\Http\Controllers;

use App\Http\Requests\CancelGiftRequest;
use App\Services\Gifts\Gifts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getGift(Gifts $gifts)
    {
        $gift = $gifts->getRandom();

        Session::put('gift', $gift->getGiftData());
        Session::put('gifted', true);
        return redirect()->back();
    }

    public function cancelGift(CancelGiftRequest $request, Gifts $gifts)
    {
        $giftClass = $request->get('gift_type');
        $gift = new $giftClass();
        $gift->cancelLastPresent($request->get('entity_id'));
        Session::remove('gift');
        return redirect()->route('home');
    }
}
