<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTransaction extends Model
{
    const TYPE_GIFT = 'gift';
    const TYPE_CANCEL = 'cancel';

    protected $fillable = [
        'amount',
        'type'
    ];
}
