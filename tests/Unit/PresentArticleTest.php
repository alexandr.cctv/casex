<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/8/18
 * Time: 7:39 PM
 */
namespace Tests\Feature;

use App\Models\Article as ArticleModel;
use App\Services\Gifts\GiftsCreators\Article;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PresentArticleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider provider
     */
    public function testCanPresentObject($available, $result)
    {
        factory(ArticleModel::class)->create([
            'available' => $available,
        ]);
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $objectCreator = new Article();
        if ($result) {
            $this->assertTrue($objectCreator->canPresent());
        } else {
            $this->assertFalse($objectCreator->canPresent());
        }
    }

    public function provider()
    {
        return [
            [
                'available' => 0,
                'result' => false
            ],
            [
                'available' => 10,
                'result' => true
            ],
        ];
    }
}
