<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 12/8/18
 * Time: 8:12 PM
 */
namespace Tests\Feature;

use App\Services\Gifts\GiftsCreators\Point;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PresentPointTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider provider
     */
    public function testCanPresentMoney()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $moneyCreator = new Point();

        $this->assertTrue($moneyCreator->canPresent());
    }

    public function provider()
    {
        return [
            [
                'result' => true
            ]
        ];
    }
}
