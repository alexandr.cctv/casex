<?php

namespace Tests\Feature;

use App\Models\MoneyGift;
use App\Services\Gifts\GiftsCreators\Money;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PresentMoneyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider provider
     */
    public function testCanPresentMoney($sum, $giftedSum, $result)
    {
        factory(MoneyGift::class)->create([
            'sum' => $sum,
            'gifted_sum' => $giftedSum
        ]);
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $moneyCreator = new Money();
        if ($result) {
            $this->assertTrue($moneyCreator->canPresent());
        } else {
            $this->assertFalse($moneyCreator->canPresent());
        }
    }

    public function provider()
    {
        return [
            [
                'sum' => 10,
                'gifted_sum' => 10,
                'result' => false
            ],
            [
                'sum' => 10,
                'gifted_sum' => 10,
                'result' => false
            ],
            [
                'sum' => 100,
                'gifted_sum' => 10,
                'result' => true
            ]
        ];
    }
}
