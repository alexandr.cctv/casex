@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(session('gift'))
                            <p>{{session('gift')['message']}}</p>
                            <form method="post" action="{{route('cancel.gift')}}">
                                @csrf
                                <input type="text" name="gift_type" value="{{session('gift')['class']}}" hidden>
                                <input type="number" name="entity_id" value="{{session('gift')['entity_id']}}" hidden>
                                <button type="submit">Cancel</button>
                            </form>
                        @elseif(!session('gifted'))
                            <a href="{{route('get.gift')}}" class="btn btn-info">Get gift!!!</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
