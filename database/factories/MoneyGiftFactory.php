<?php

use Faker\Generator as Faker;

$factory->define(App\Models\MoneyGift::class, function (Faker $faker) {
    $amount = $faker->randomFloat();
    return [
        'sum' => $amount,
        'gifted_sum' => $faker->randomFloat(null,0, $amount),
        'created_at' => $faker->dateTimeThisDecade,
        'updated_at' => $faker->dateTimeThisDecade,
    ];
});
